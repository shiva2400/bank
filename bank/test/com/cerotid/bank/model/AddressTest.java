package com.cerotid.bank.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AddressTest {
	
	private Address address;

	@Before
	public void setUp() throws Exception {
		
		address = new Address("7834 Brianna Dr.", "43004",
							"Blacklick", "OH");			
	}

	@Test
	public void testAddress() {
		assertEquals("7834 Brianna Dr.", address.getStreetName());
		assertEquals("43004", address.getZipCode());
		assertEquals("Blacklick", address.getCity());
		assertEquals("OH", address.getStateCode());
		
	}

}
