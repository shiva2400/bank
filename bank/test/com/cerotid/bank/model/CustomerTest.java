package com.cerotid.bank.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {
	
	private Address address;
	private ArrayList<Account> accounts;
	private Customer customer;

	@Before
	public void setUp() throws Exception {
		address = new Address("7834 Brianna Dr.", "43004",
				"Blacklick", "OH");	
		accounts = new ArrayList<Account>();
		customer = new Customer("Shiva", "Acharya", accounts, address, "1234");
	}

	@Test
	public void test() {
		assertEquals(0, accounts.size());
		assertEquals("Shiva", customer.getFirstName());
		assertEquals("Acharya", customer.getLastName());
		
		Account account = new Account(AccountType.Checking, 200.00);
		accounts.add(account);
		assertEquals(1, accounts.size());
		assertEquals("Checking", customer.getAccounts().get(0).getAccountType());
	}

}
