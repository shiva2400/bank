package com.cerotid.bank.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AccountTest {
	
	private Account account;

	@Before
	public void setUp() throws Exception {
		
		account = new Account(AccountType.Checking, 200.00);
	}

	@Test
	public void testGetAccountType() {
		
		assertEquals("Checking", account.getAccountType());
	}

}
