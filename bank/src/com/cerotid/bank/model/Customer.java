package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer implements Comparable<Customer>{

	private String ssn;
	private String firstName;
	private String lastName;
	ArrayList<Account> accounts;
	private Address address;

	public Customer(String firstName, String lastName, ArrayList<Account> accounts, Address address, String ssn) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.accounts = accounts;
		this.address = address;
		this.ssn = ssn;
		
	}
	
	public String getSsn() {
		return this.ssn;
	}
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void printCustomerAccounts() {
		System.out.println("Customer Accounts: ");
		for (Account a : accounts) {
			a.printAccountInfo();

		}
	}
	
	public void printCustomerDetails() {
		
		System.out.println("=======================================");

		String details = "Customer Details\nName : " + this.lastName +", " + this.firstName;
		System.out.println(details);
		
		printCustomerAccounts();
		
		String space = " ";
		
		String address = this.address.getStreetName() + space + this.address.getCity()
						+ space + this.address.getStateCode() + space +
						this.address.getZipCode();
		
		System.out.println("Address: " + address);
		
		System.out.println("=======================================");
	}

	@Override
	public int compareTo(Customer o) {
		
		return this.getLastName().compareTo(o.getLastName());
	}

}
