package com.cerotid.bank.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;


public class Bank {
	
	private String bankName;
	ArrayList<Customer> customers;
	
	
	
	public Bank(String bankName, ArrayList<Customer> customers) {
		this.bankName = bankName;
		this.customers = customers;
	}
	
	

	public String getBankName() {
		return bankName;
	}



	public void setBankName(String bankName) {
		this.bankName = bankName;
	}



	public ArrayList<Customer> getCustomers() {
		return customers;
	}



	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}



	public void printBankName() {
		System.out.println(getBankName());
	}
	
	public void printBankDeails() {
		System.out.println(getBankName());
		System.out.println("Customers:");
		
		for(Customer c: customers) {
			System.out.println("\t-" + c.getLastName() + "," + c.getFirstName());
		}
	}
	
	
	public void sortCustomer() {
		
		
		BufferedReader reader = null;
		FileWriter output = null;
		
		try {
			reader = new BufferedReader(new FileReader("/Users/shiva/Desktop/JSP-For_Beginners/workplace/bank/customers.txt"));
			
			String name;
			
			while((name = reader.readLine()) != null) {
				String [] nameSplit = name.split(",");
				Customer c = new Customer(nameSplit[1], nameSplit[0], null, null, null);
				customers.add(c);
			}
			
			Collections.sort(customers);
			
			output = new FileWriter("/Users/shiva/Desktop/JSP-For_Beginners/workplace/bank/customers2.txt");
			
			for (Customer custo : customers) {
				output.write(custo.getLastName() + ", " + custo.getFirstName() + "\n");		
			}
			
			reader.close();
			output.close();
			
		} catch (IOException e) {
			System.out.println("Error reading file.");
		}
	}
}
