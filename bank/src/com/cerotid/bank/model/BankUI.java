package com.cerotid.bank.model;

import java.util.ArrayList;
import java.util.Scanner;

import com.cerotid.bo.*;

public class BankUI {

	public void display() {

		Scanner input = new Scanner(System.in);
		
		BankBO bank = new BankBOImpl();
		
		boolean keepLooping = true;
		
		while (keepLooping) {
			System.out.print("Choose from the menow below:\n\t1. Add Customer\n\t2. Add Account "
					+ "\n\t3. Send Money\n\t4. Print Bank Status\n\t5. Print All Customers By State\n\t6.Exit");
			int in = input.nextInt();

			switch (in) {
			case 1:
				System.out.print("Enter Customer First Name: ");
				String firstName = input.next();
				System.out.print("Enter Customer Last Name: ");
				String lastName = input.next();
				System.out.print("Enter Customer Street Address: ");
				String street = input.nextLine();
				input.nextLine();
				System.out.print("Enter Zip Code: ");
				String zipCode = input.next();
				System.out.print("Enter Customer City: ");
				String city = input.nextLine();
				input.nextLine();
				System.out.print("Enter Customer State Code : ");
				String state = input.next();
				System.out.print("Enter Customer ssn : ");
				String ssn = input.next();
				
				Address a = new Address(street, zipCode, city, state);
				bank.addCustomer(new Customer(firstName, lastName, new ArrayList<Account>(), a, ssn));
				break;
				
			case 2: 
				System.out.print("Enter social security number to add account : ");
				String social = input.next();
				Customer c = bank.getCustomerInfo(social);
				if (c == null) {
					System.out.println("Social Security does not match");
					break;
				}
				System.out.print("Enter Account Type : \n\t1. Checking\n\t2. Saving\n\t3. Business-Checking ");
				int accountTypeNumber = input.nextInt();
				AccountType accountType = null;
				if (accountTypeNumber == 1) {
					accountType = AccountType.Checking;
				} else if (accountTypeNumber == 2){
					accountType = AccountType.Saving;
				} else if (accountTypeNumber == 3) {
					accountType = AccountType.Business_Checking;
				}
				System.out.print("Enter initial balance: ");
				double initialBalance = input.nextDouble();
				
				Account account = new Account(accountType, initialBalance);
				
				bank.openAccount(c, account);
				break;
				
			case 3: 
				System.out.print("How much do you want to send? ");
				double sendMoney = input.nextDouble();
				break;
				
			case 4: 
				bank.printBankStatus();
				break;
				
			case 5: 
				System.out.print("Enter state code: ");
				String stateC = input.next();
				
				bank.getCustomersByState(stateC);
				break;
				
			case 6: 
				System.out.println("Exiting the System! Thank you for your time.");
				keepLooping = false;
			}
			

			System.out.print("Choose from the menow below:\n\t1. Add Customer\n\t2. Add Account "
					+ "\n\t3. Send Money\n\t4. Print Bank Status\n\t5. Print All Customers By State\n\t6.Exit");
			in = input.nextInt();
		}
		
		input.close();
	}

}
