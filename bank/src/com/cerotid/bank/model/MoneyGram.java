package com.cerotid.bank.model;

enum DeliveryOptions {
	_10Minute, _24HRS;
}

public class MoneyGram extends Transaction{
	
	private DeliveryOptions options;
	private String destinationCountry;
	
	public MoneyGram(DeliveryOptions options, String destinationCountry, double amount, double fee, String receiverFirstName, String receiverLastName) {
		super(amount, fee, receiverFirstName, receiverLastName);
		this.options = options;
		this.destinationCountry = destinationCountry;
	}

	
	
	public DeliveryOptions getOptions() {
		return options;
	}



	public void setOptions(DeliveryOptions options) {
		this.options = options;
	}



	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	
}
