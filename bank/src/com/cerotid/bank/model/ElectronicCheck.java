package com.cerotid.bank.model;

enum TypeOfCheck {
	paperCheck, eCheck;
}

public class ElectronicCheck extends Transaction{
	
	private TypeOfCheck checkType;

	public ElectronicCheck(double amount, double fee, String receiverFirstName, String receiverLastName,
			TypeOfCheck checkType) {
		super(amount, fee, receiverFirstName, receiverLastName);
		this.checkType = checkType;
	}

	public TypeOfCheck getCheckType() {
		return checkType;
	}

	public void setCheckType(TypeOfCheck checkType) {
		this.checkType = checkType;
	}
	
	
}
