package com.cerotid.bank.model;

public class WireTransfer extends Transaction{
	
	private String beneficiaryFirstName;
	private String beneficiaryLastName;
	private String intermediaryBankSWIFTCode;
	private String beneficiaryBankName;
	private String beneficiaryAccountNumber;
	
	public WireTransfer(double amount, double fee, String receiverFirstName, String receiverLastName,
			String beneficiaryFirstName, String beneficiaryLastName, String intermediaryBankSWIFTCode,
			String beneficiaryBankName, String beneficiaryAccountNumber) {
		super(amount, fee, receiverFirstName, receiverLastName);
		this.beneficiaryFirstName = beneficiaryFirstName;
		this.beneficiaryLastName = beneficiaryLastName;
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
		this.beneficiaryBankName = beneficiaryBankName;
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	public String getBeneficiaryFirstName() {
		return beneficiaryFirstName;
	}

	public void setBeneficiaryFirstName(String beneficiaryFirstName) {
		this.beneficiaryFirstName = beneficiaryFirstName;
	}

	public String getBeneficiaryLastName() {
		return beneficiaryLastName;
	}

	public void setBeneficiaryLastName(String beneficiaryLastName) {
		this.beneficiaryLastName = beneficiaryLastName;
	}

	public String getIntermediaryBankSWIFTCode() {
		return intermediaryBankSWIFTCode;
	}

	public void setIntermediaryBankSWIFTCode(String intermediaryBankSWIFTCode) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}
	

}
