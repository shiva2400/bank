package com.cerotid.bank.model;

import java.io.FileWriter;

public class PrintWriter {

	public static void main(String[] args) {
		
		FileWriter writer = null;
		
		try {
			writer = new FileWriter("/Users/shiva/Desktop/JSP-For_Beginners/workplace/bank/customer.txt");
			writer.write("Acharya, Shiva");
			writer.write("Baral, Kumar");
			writer.write("Uprety, Bishnu");
			writer.write("Acharya, Ram");
			writer.write("Karki, Kumar");
			writer.write("Dangal, Bishnu");
			
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
