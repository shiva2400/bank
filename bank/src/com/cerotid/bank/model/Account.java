package com.cerotid.bank.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Random;

enum AccountType {
	
	Checking, Saving, Business_Checking;
	
}

public class Account{
	

	private AccountType accountType;
	private LocalDate accountOpenDate;
	private LocalDate accountCloseDate;
	private double amount;
	private String accountNumber;
	

	public Account(AccountType accountType, double amount) {
		this.accountType = accountType;
		accountOpenDate = LocalDate.now();
		accountCloseDate = null;
		this.amount = amount;
		
		Random random = new Random();
		
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < 10; i++) {
			int x = random.nextInt(10);
			sb.append(x);
		}
		
		this.accountNumber = sb.toString();
	}
	

	public AccountType getAccountType() {
		return this.accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
	
	
	public LocalDate getAccountOpenDate() {
		return accountOpenDate;
	}


	public void setAccountOpenDate(LocalDate accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}


	public LocalDate getAccountCloseDate() {
		return accountCloseDate;
	}


	public void setAccountCloseDate(LocalDate accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getAccountNumber() {
		return accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public void printAccountInfo() {
		System.out.println("\t-" + getAccountType());
	}

}
