package com.cerotid.bo;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.cerotid.bank.model.*;


public class BankBOImpl implements BankBO {

	private Bank bank;
	
	public BankBOImpl() {
		
		this.bank = new Bank("Cerotid", new ArrayList<Customer>());
	
	}
	
	public void print() {
		System.out.println(bank.getCustomers());
	}


	@Override
	public void addCustomer(Customer customer) {
		
		bank.getCustomers().add(customer);
		
		this.bank.getCustomers().get(0).printCustomerDetails();

	}

	@Override
	public boolean openAccount(Customer customer, Account account) {
		
		for (Customer cust : bank.getCustomers()) {
			if (cust.getSsn().equals(customer.getSsn())) {
				cust.getAccounts().add(account);
				return true;
			}
		}
		return false;
	}

	@Override
	public void sendMoney(Customer customer, Account account, Transaction transaction) {
		
		for(Account acct : customer.getAccounts()) {
			if (acct.getAccountNumber().contentEquals(account.getAccountNumber())) {
				double amount = transaction.getAmount();
				double fee = transaction.getFee();
				double totalTransaction = amount + fee;
				account.setAmount(account.getAmount() - totalTransaction);
			}
		}

	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Enter amount to deposit to this account: ");
		double amount = in.nextDouble();
		
		customer.getAccounts().get(0).setAmount(amount);

	}

	@Override
	public void editCustomerInfo(Customer customer) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter:\n\t1. To Edit First Name\n\t2. To Edit Last Name");
		int response = input.nextInt();
		
		switch(response) {
		case 1:
			System.out.print("Enter First Name: ");
			String firstName = input.nextLine();
			customer.setFirstName(firstName);
			break;
			
		case 2:
			System.out.print("Enter Last Name: ");
			String lastName = input.nextLine();
			customer.setFirstName(lastName);
			break;
			
		default: 
			System.out.println("request unsuccessful");
		}

	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		Customer customer = null;
		
		for (Customer c : this.bank.getCustomers()) {
			if (c.getSsn().equals(ssn)) {
				customer = c;
			}
		}
		return customer;
	}

	@Override
	public void printBankStatus() {
		
		this.bank.printBankDeails();

	}

	@Override
	public void serializeBank(){
		
		
		 String fileName = "customers.txt";
		 
		 try {
		
			 FileInputStream in = new FileInputStream(fileName);	
			 ObjectInputStream oIn = new ObjectInputStream(in);	
			 oIn.readObject();
			 
		 } catch(Exception e) {
			 e.printStackTrace();
		 }

	}

	@Override
	public List<Customer> getCustomersByState(String stateCode) {
		
		List<Customer> customer = new ArrayList<Customer>();
		
		for (Customer c : this.bank.getCustomers()) {
			if (c.getAddress().getStateCode().equalsIgnoreCase(stateCode)) {
				customer.add(c);
			}
		}
		
		return customer;
	}

}
